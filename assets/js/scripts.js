jQuery(document).ready(function($){
    $('form#swl_sync_form').on('submit', function(e){
        e.preventDefault();
        var that = $(this),
            url = ajax_variables.ajax_url,
            organization = $('.organization').val();
        $.ajax({
            url: url,
            type:"POST",
            data: {
                organization: organization,
                action: 'sync_products'
            },
            beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $('#loader').removeClass('hidden');
                $('#result-sync').html("Loading...");
            },
            success: function (data) {
                // console.log(data);
                $('#result-sync').html(data);
            },
            error: function (e) {
                $('#result-sync').html(e);
            },
            complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
                $('#loader').addClass('hidden')
            },
        });
        // $('.ajax')[0].reset();
    });
    $('.get_organizations').on('click', function(e) {
        e.preventDefault();
        var that = $(this);
        $.ajax({
            type: 'GET',
            url: ajax_variables.ajax_url,
            data: {
                action: 'get_organizations',
                type_request:'get_organizations'
            },
            beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $('#loader').removeClass('hidden');
                $('#result-sync').html("Loading...");
            },
            success: function (data) {
                $('#result-sync').html(data);
                // window.location.reload();
            },
            error: function (e) {
                $('#result-sync').html(e);
            },
            complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
                $('#loader').addClass('hidden')
            },
        });
    });
});