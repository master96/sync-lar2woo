<?php


class SWL_To_Wp_Site
{
    protected $url;
    protected $username;
    protected $password;

    public function __construct()
    {
        $this->url = get_option('swl_settings_url');
        $this->username = get_option('swl_settings_login');
        $this->password = get_option('swl_settings_password');
    }

    public function getProducts($organization_id)
    {
        $wc_product_ids = [];
        $url = $this->url.'/api/minisite/'.$organization_id.'/products';
        $login = $this->login();
        if (!isset($login['token'])) {
            print_r($login['message']);
            die();
        }
        $params = array('headers' => array(
            'Authorization' => 'Bearer ' . $login['token']
        ));
        $response = wp_remote_get( $url, $params );
        $products = json_decode($response['body']);
        foreach ($products->data as $product)
        {
            $category = $this->getCategory($product->category_id);

            $getProduct = $this->get_post_by_meta(['meta_key' => 'old_id', 'meta_value' => $product->id]);
            if ($getProduct) {
                $wc_product_ids[] = $this->updateProduct($product, $category, $getProduct->ID );
            } else {
                $wc_product_ids[] = $this->createProduct($product, $category);
            }
        }

        print_r("The request was completed successfully.");
        die();


    }

    public function getCategory($cat_id)
    {
        $url = $this->url.'/api/categories/'.$cat_id;
        $login = $this->login();
        if (!$login['token']) {
            return $login;
        }
        $params = array('headers' => array(
            'Authorization' => 'Bearer ' . $login['token']
        ));
        $response = wp_remote_get( $url, $params );
        $response = json_decode($response['body']);
        $args = array(
            'taxonomy'  => 'product_cat',
            'hide_empty' => false, // also retrieve terms which are not used yet
            'meta_query' => array(
                array(
                    'key'       => 'old_id',
                    'value'     => "$cat_id",
                    'compare'   => 'LIKE'
                )
            ),
        );
        $terms = get_terms( $args );
        if (empty($terms)) {
            $wpdocs_cat_id = wp_insert_term('[:ru]' .$response->data->name_ru. '[:uz]' .$response->data->name_uz, 'product_cat');
            add_term_meta(
                $wpdocs_cat_id['term_id'],
                'old_id',
                sanitize_text_field($cat_id)
            );

            return $wpdocs_cat_id['term_id'];
        }else{
            return $terms[0]->term_id;
        }
    }

    public function createProduct($data, $category_id)
    {
        $attach_image = new SWL_Attach_Media();
        if ($data->publish_status === 1 && $data->status === 1) {
            $publish_status = 'publish';
        } else {
            $publish_status = 'pending';
        }
        if ($data->availability_option_id = 2) {
            $stock_status = 'outofstock';
        } elseif ($data->availability_option_id = 3) {
            $stock_status = 'onbackorder';
        } else {
            $stock_status = 'instock';
        }
        $post_args = array(
//        'post_author' => intval( $product['author_id'] ), // The user's ID
            'post_title' => sanitize_text_field( "[:ru]" .$data->name_ru . "[:uz]" . $data->name_uz . "[:]" ), // The product's Title
            'post_type' => 'product',
            'post_status' => $publish_status, // This could also be $data['status'];
            'post_content' => sanitize_text_field( '[:ru]' . $data->details->text_ru . '[:uz]' . $data->details->text_uz . '[:]' ),
            'meta_input' => [
                'old_id' => $data->id
            ]
        );

        $post_id = wp_insert_post( $post_args );

        if ( !empty( $post_id ) && function_exists( 'wc_get_product' ) ) {

            if (!empty($data->image)) {

                $attach_image->swl_upload_from_url( $this->url . '/upload/' .$data->image, $post_id, true,  'image');

            }
            if (count($data->images) !== 0 ) {

                if (empty($data->image))
                    $attach_image->swl_upload_from_url( $this->url . '/upload/' .$data->images[0], $post_id, true, 'image');

                foreach ($data->images as $image) {

                    $attach_image->swl_upload_from_url( $this->url . '/upload/' .$image->src, $post_id, true, 'gallery');

                }
            }
            $product = wc_get_product( $post_id );
            if ($data->is_price_from === 1) {
                $is_price_from ='yes';
            } else {
                $is_price_from = 'no';
            }
            update_post_meta( $post_id, '_price_is_from', $is_price_from);
            $product->set_sku( 'pre-' . $post_id ); // Generate a SKU with a prefix. (i.e. 'pre-123')
            $product->set_stock_status($stock_status);
            if (isset($data->price) && (float)$data->price > 0)
                $product->set_regular_price( $data->price );
            $product->set_regular_price( $data->price ); // Be sure to use the correct decimal price.
            $product->set_category_ids( array( $category_id ) ); // Set multiple category ID's.
            $product->set_stock_status($stock_status);
            $product->save(); // Save/update the WooCommerce product object.
        }
        return $post_id;
    }

    public function updateProduct($data, $category_id, $product_id)
    {

        $attach_image = new SWL_Attach_Media();
        $media = get_attached_media( '', $product_id );
        $images_names = [];
        foreach ($media as $image) {
            $images_names[] = $image->post_title;
        }

        if ($data->publish_status === 1 && $data->status === 1) {
            $publish_status = 'publish';
        } else {
            $publish_status = 'pending';
        }
        if ($data->availability_option_id = 2) {
            $stock_status = 'outofstock';
        } elseif ($data->availability_option_id = 3) {
            $stock_status = 'onbackorder';
        } else {
            $stock_status = 'instock';
        }

        $product = [
            'ID' => $product_id,
            'post_title' => sanitize_text_field( "[:ru]" .$data->name_ru . "[:uz]" . $data->name_uz . "[:]" ), // The product's Title
            'post_type' => 'product',
            'post_status' =>  $publish_status, // This could also be $data['status'];
            'post_content' => sanitize_text_field( '[:ru]' . $data->details->text_ru . '[:uz]' . $data->details->text_uz . '[:]' ),
            'meta_input' => [
                'old_id' => $data->id
            ]

        ];
        $image_name = explode('/', $data->image);
        if (!in_array($image_name[3], $images_names)) {
            $attach_image->swl_upload_from_url( $this->url . '/upload/' .$data->image, $product_id, 'image');
        }
        foreach ($data->images as $image_gallery) {
            $gallery_image_name = explode('/', $image_gallery->src);

            if (!in_array($gallery_image_name[3], $images_names)) {

                $attach_image->swl_upload_from_url( $this->url . '/upload/' .$image_gallery->src, $product_id, 'gallery');

            }
        }
        wp_update_post( $product );
        if ($data->is_price_from === 1) {
            $is_price_from ='yes';
        } else {
            $is_price_from = 'no';
        }
        $product = wc_get_product( $product_id );
        update_post_meta( $product_id, '_price_is_from', $is_price_from);
        $product->set_stock_status($stock_status);
        if (isset($data->price) && (float)$data->price > 0)
            $product->set_regular_price( $data->price ); // Be sure to use the correct decimal price.
        $product->set_category_ids( array( $category_id ) ); // Set multiple category ID's.
        $product->save();
        return $product_id;
    }

    /* Get user organizations */
    public function getOrganizations()
    {
        $login = $this->login();
        if (isset($login['token'])) {
            $params = array('headers' => array(
                'Authorization' => 'Bearer ' . $login['token']
            ));
            $url = $this->url . '/api/organizations';
            $organizations = wp_remote_get( $url, $params );
            return json_decode($organizations['body']);
        } else {
            print_r($login->message);
            die();
        }
    }

    public function login()
    {
        $inactive = 300;
        if (get_option('swl_login_token_time') && !empty(get_option('swl_login_token_time'))) {
            $token_start_time = get_option('swl_login_token_time');
        } else {
            $token_start_time = 1;
        }
        $token_life = time() - $token_start_time;
        if ($token_life < $inactive ) {
            $result = [
                'token' => get_option('swl_login_token'),
            ];
        } else {
            $url = $this->url.'/api/login';
            $params = [
                'body' => [
                    'email' => $this->username,
                    'password' => $this->password
                ],
                'httpversion' => '1.0',

                ];
            $request = wp_remote_post( $url, $params );
            $response = json_decode($request['body']);
            if ($response->token) {
                $result =  [
                    'token' => $response->token
                ];
                update_option('swl_login_token', $response->token);
                update_option('swl_login_token_time', time());
            } else {
                $result = $response;
            }
        }
        return $result;
    }

    private function get_post_by_meta( $args = array() )
    {

        $args = ( object )wp_parse_args( $args );
        $args = array(
            'meta_query'        => array(
                array(
                    'key'       => $args->meta_key,
                    'value'     => $args->meta_value
                )
            ),
            'post_type'         => 'product',
            'post_status' => 'any',
            'posts_per_page'    => '1'
        );

        // run query ##
        $posts = get_posts( $args );

        // check results ##
        if ( ! $posts || is_wp_error( $posts ) ) return false;

        // test it ##
        #pr( $posts[0] );

        // kick back results ##
        return $posts[0];

    }
}