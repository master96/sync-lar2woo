<?php
/**
 * @package Sync with Laravel site
 */
/*
 * Plugin Name: Sync with Laravel site
 * Description: This plugin is for make Sync woocommerce with Laravel site.
 * Version: 1.0
 * Author: G'iyosiddin
 **/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access.
}

final class Sync_With_Laravel
{
    /**
     * Current plugin version
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * Init
     *
     * @since 1.0
     */
    public static function init() {
        $plugin = new self();
    }

    public function __construct()
    {
        $this->defination();
        $this->includes();
        //add actions
        add_action( 'admin_init', array($this, 'swl_options_add' ));
        add_action('admin_enqueue_scripts', array($this,'register_scripts'));
        add_action( 'admin_menu', array($this, 'add_options_menu'));
        add_action( 'product_cat_add_form_fields', array($this, 'oldId_add_term_fields') );
        add_action( 'woocommerce_product_options_general_product_data', array($this, 'swl_create_custom_field') );
        add_action( 'product_cat_edit_form_fields', array($this, 'product_cat_edit_term_fields', 10, 2) );
        add_action( 'wp_ajax_get_organizations', array($this, 'get_user_organizations') );
        add_action( 'wp_ajax_sync_products', array($this, 'sync_products') );
        add_action( 'product_cat_edit_form_fields', array($this, 'product_cat_edit_term_fields') );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
        add_action('woocommerce_single_product_summary', array($this, 'show_product_categories'), 100);
        add_filter( 'woocommerce_get_price_html', array($this,'custom_price_message' ));
        add_action('woocommerce_product_options_general_product_data', array($this,'product_custom_price_add'));
        add_action('woocommerce_process_product_meta', array($this,'product_custom_price_save'));
    }
    private function defination()
    {
        define( 'SWL__MINIMUM_WP_VERSION', '5.0' );
        define( 'SWL__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
    }

    private function swl_classes()
    {
        $classes = array(
            'SWL_To_Wp_Site'    => SWL__PLUGIN_DIR . 'Classes/to-wp.class.php',
            'SWL_From_Wp_Site'  => SWL__PLUGIN_DIR . 'Classes/from-wp.class.php',
            'SWL_Attach_Media'  => SWL__PLUGIN_DIR . 'Classes/attach-image.class.php',
        );

        return $classes;
    }

    private function includes()
    {
        foreach ($this->swl_classes() as $id => $path) {
            if (is_readable($path) && !class_exists($id)) {
                include_once $path;
            }
        }
    }

    /* Register css and scripts */
    public function register_scripts()
    {
        wp_enqueue_script('swl-scripts', plugins_url('/assets/js/scripts.js', __FILE__ ));
        wp_enqueue_script('bootstrap-script', plugins_url('/assets/bootstrap-4.6.1/js/bootstrap.min.js', __FILE__ ));
        wp_enqueue_style( 'swl-styles', plugins_url( '/assets/css/style.css', __FILE__ ));
        wp_enqueue_style( 'bootstrap-style', plugins_url('/assets/bootstrap-4.6.1/css/bootstrap.min.css', __FILE__ ));
        wp_localize_script("swl-scripts", 'ajax_variables', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ));
        wp_enqueue_script("swl-scripts");
    }

    public function swl_options_add(){
        register_setting( 'swl_settings', 'swl_settings_url');
        register_setting( 'swl_settings', 'swl_settings_login');
        register_setting( 'swl_settings', 'swl_settings_password');
        register_setting( 'swl_settings', 'organizations_list');
        register_setting( 'swl_settings', 'swl_login_token');
        register_setting( 'swl_settings', 'swl_login_token_time', [
            'default' => '1',
        ] );
    }

//add settings page to menu
    public function add_options_menu() {
        add_menu_page( __( 'Sync Prom.uz' ), __( 'Sync Prom.uz' ), 'manage_options', 'swl_settings', array($this, 'swl_options_page'));
    }
    //start settings page
    public function swl_options_page() {
        $organizations = get_option('organizations_list');
        ?>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <form method="post" class="custom_options" action="options.php">
                        <h2>Another site options</h2>
                        <?php settings_fields( 'swl_settings'); ?>
                        <div class="row">
                            <table class="col">
                                <tr >
                                    <td class="p-3"><span><?php _e( 'Site url' ); ?></span><br>
                                        <input id="swl_settings_url" type="text" size="36" name="swl_settings_url" value="<?php esc_attr_e( get_option( 'swl_settings_url' ) ); ?>" />
                                        <!--                                <label for="swl_settings_url">--><?php //_e( 'Enter site url which wanted to make sync ' ); ?><!--</label>-->
                                    </td>
                                </tr>
                                <tr >
                                    <td class="p-3"><span><?php _e( 'Login' ); ?></span><br>
                                        <input id="swl_settings_login" type="text" size="36" name="swl_settings_login" value="<?php esc_attr_e( get_option( 'swl_settings_login' ) ); ?>" />
                                        <!--                                <label for="swl_settings_login">--><?php //_e( 'Enter your login ' ); ?><!--</label>-->
                                    </td>
                                </tr>
                                <tr >
                                    <td class="p-3"><span><?php _e( 'Password' ); ?></span><br>
                                        <input id="swl_settings_password" type="password" size="36" name="swl_settings_password" value="<?php esc_attr_e( get_option( 'swl_settings_password' ) ); ?>" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <p><input name="submit" id="submit" value="Save Changes" class="black-btn mt-2" type="submit"></p>
                    </form>
                </div><!-- END wrap -->
                <div class="col-6">
                    <form method="post" action="" id="swl_sync_form" class="col-12">
                        <h2>Sync Options</h2>
                        <input type="hidden" name="make_sync" value="true" />
                        <fieldset class="form-group row">
                            <div class="form-check col-6">
                                <select class="custom-select required custom-select-lg mt-2 organization" name="organization_id">
                                    <?php foreach ($organizations as $organization) : ?>
                                        <option value="<?= $organization->id; ?>"><?=$organization->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-check col-6">
                                <button class="black-btn mt-2 get_organizations" >Get or update organizations</button>
                            </div>
                        </fieldset>
                        <p><input class="btn btn-success black-btn sync" name="submit" id="submit" value="Make sync" type="submit"></p>
                        <div id="result-sync"></div>
                        <div id="loader" class="lds-dual-ring hidden"></div>
                    </form>
                </div><!-- END wrap -->
            </div>
        </div>
        <?php
    }

    /**
     * Display the custom text field
     * @since 1.0.0
     */
    public function swl_create_custom_field() {
        $args = array(
            'id' => 'old_id',
            'label' => __( 'Product id which saved to another site.', 'swl_old_id' ),
            'class' => 'sync-custom-field',
            'desc_tip' => true,
            'description' => __( 'It is old id which imported product which saved to another site.', 'swl_old_id' ),
        );
        woocommerce_wp_text_input( $args );
    }

    public function oldId_add_term_fields( $taxonomy ) {

        echo '<div class="form-field">
	<label for="swl-product_old_id">Old id</label>
	<input type="text" name="old_id" id="old-id" />
	<p>Old id which saved on another site.</p>
	</div>';
    }

    function product_cat_edit_term_fields( $term ) {

        $value = get_term_meta( $term->term_id, 'old_id', true );

        echo '<div class="form-field">
            <label for="misha-text">Old id</label>
            <input type="text" name="old_id" id="old-id" value="' . esc_attr( $value ) .'" />
                <p>Old id which saved on anosher site.</p>
            </td>
            </tr>';

    }

    public function get_user_organizations()
    {
        $toWp = new SWL_To_Wp_Site();
        $organizations = $toWp->getOrganizations();
        if ($organizations->data) {
            update_option('organizations_list', $organizations->data);
            print_r("The request was completed successfully");
            die();
        } else {
            return $organizations->message;
        }
    }

    public function sync_products()
    {
        $to_Wp = new SWL_To_Wp_Site();
        $organization_id = filter_var($_POST['organization'],FILTER_SANITIZE_NUMBER_INT);
        $to_Wp->getProducts($organization_id);
        return 'successful';
    }

    function show_product_categories() {
        global $product;
        echo __(wc_get_product_category_list( $product->get_id(), ', ', '' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '' ),'text-domain');
    }


    function custom_price_message($price) {
        global $post;
        if (!empty($price)) {
            if ( is_product() && get_post_meta( $post->ID, '_price_is_from', true ) == 'yes' ) {
                echo __("[:uz]Boshlanish narxi: [:ru]Цена в формате от: [:]") . $price;
            } else {
                return $price;
            }
        } else {
            return __("[:uz]Narx kelishiladi.[:ru]Цена договорная[:]");
        }
    }
    // Display Fields
    function product_custom_price_add() {
        global $post;

        echo '<div class="product_custom_field">';

        // Custom Product Checkbox Field
        woocommerce_wp_checkbox( array(
            'id'        => '_price_is_from',
            'desc'      => __('Price is from this price', 'woocommerce'),
            'label'     => __('Price is from', 'woocommerce'),
            'desc_tip'  => 'true'
        ));

        echo '</div>';
    }

    // Save Fields
    function product_custom_price_save($post_id) {
        // Custom Product checkbox Field
        $engrave_text_option = isset( $_POST['_price_is_from'] ) ? 'yes' : 'no';
        update_post_meta($post_id, '_price_is_from', esc_attr( $engrave_text_option ));
    }
}

/* Init plugin */
add_action( 'plugins_loaded', array( 'Sync_With_Laravel', 'init' ), 10 );

