=== Sync with Laravel site ===
 Contributors: promdev
 Tags: sync, sync woocommerce with laravel, synchronize, synchronize with laravel,
 Description: This plugin is for make Sync woocommerce with Laravel site.
 Version: 1.0
 Requires at least: 5.0
 Tested up to: 6.0
 Author: G'iyosiddin
 Stable tag: 1.0
 Requires PHP: 7.0
 License: GPLv2 or later
 License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress plugin to make synchronize woocommerce with Laravel site.

== Description ==

This simple plugin is for make synchronizing Woocommerce with the Laravel site. For working this plugin mast to install several plugins such as Woocommerce and Q-Translate.

== Installation ==

1. Upload the plugin folder to your /wp-content/plugins/ folder.
1. Go to the **Plugins** page and activate the plugin.

== Frequently Asked Questions ==

= How do I use this plugin? =
For use this plugin required install Woocommerce.
After installing the plugin, you need to go to plugin settings and need write the site link which you want to make synchronize and login and password for login into this site.

= How to uninstall the plugin? =

Simply deactivate and delete the plugin.

== Screenshots ==
1. Description of the first screenshot.
1. Description of the second screenshot.

== Changelog ==
= 1.0 =
* Plugin released.